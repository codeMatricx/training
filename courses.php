
<!DOCTYPE html>
<html lang="en">
<head>
<title>Emphasize an Education Category Bootstrap Responsive Web Template | Codes :: w3layouts</title>
<!-- custom-theme -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Emphasize Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, Sony Ericsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- // custom-theme -->
<!--css links-->
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" /><!--bootstrap-->
<link href="css/font-awesome.css" rel="stylesheet"><!--font-awesome-->
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" /><!--stylesheet-->
<!--//css links-->
<!--fonts-->
<link href="//fonts.googleapis.com/css?family=Raleway:200,300,400,500,600,700" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=PT+Serif:400,700" rel="stylesheet">
<script type="text/javascript" src="js/style.js"></script>
<!--//fonts-->
</head>
<body>
<!-- Header -->
<div id="home" class="banner inner-banner-w3l">
		<div class="header-nav">
			<nav class="navbar navbar-default">
			<div class="header-top">
					<div class="navbar-header logo">
								<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
									<span class="sr-only">Toggle navigation</span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</button>
								<h1>
									<a class="navbar-brand" href="index.php"><i class="fa fa-graduation-cap" aria-hidden="true"></i>WebTutorial</a>
								</h1>
					</div>
					<!-- navbar-header -->
					<div class="contact-bnr-w3-agile">
								<ul>
									<li><i class="fa fa-envelope-o" aria-hidden="true"></i><a href="mailto:info@example.com">info@example.com</a></li>
									<li><i class="fa fa-phone" aria-hidden="true"></i>+1 (100)222-0-33</li>	
								</ul>
							</div>
							</div>
					<div class="collapse navbar-collapse cl-effect-13" id="bs-example-navbar-collapse-1">

						<ul class="nav navbar-nav navbar-right">
							<li><a href="index.php" class="active">Home</a></li>
							<li><a href="about.php">About</a></li>
							<li><a href="services.php">Services</a></li>
							
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Courses<span class="caret"></span></a>
								<ul class="dropdown-menu">
									<li><a href="courses.php">PHP</a></li>
									<li><a href="courses.php">CSS</a></li>
									<li><a href="courses.php">HTML</a></li>
									<li><a href="courses.php">JAVASCRIPT</a></li>
								</ul>
							</li>
							<li><a href="contact.php">Contact</a></li>
						</ul>

					</div>
					<div class="clearfix"> </div>	
				</nav>
							<div class="clearfix"> </div>
		</div>
</div>	
<!-- //Header -->

<div class="container">
	<div class="row" style="margin-top: 25px;">
		<div class="">
		<div class="col-sm-3" style="">
			<nav class="navbar navbar-inverse sidebar" role="navigation">
 
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-sidebar-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="#">PHP</a>
		</div>
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-sidebar-navbar-collapse-1">
			<ul class="nav">
				<li class="active"><a href="#">PHP - INTRODUCTION<span style="font-size:16px;" class="pull-right hidden-xs showopacity"></span></a></li>
				<li class="active"><a href="#">PHP - SYNTAX<span style="font-size:16px;" class="pull-right hidden-xs showopacity"></span></a></li>
				<li class="active"><a href="#">PHP - VARIABLE<span style="font-size:16px;" class="pull-right hidden-xs showopacity"></span></a></li>
				<li class="active"><a href="#">PHP - CONSTANT<span style="font-size:16px;" class="pull-right hidden-xs showopacity"></span></a></li>
				<li class="active"><a href="#">PHP - OPERATOR TYPES<span style="font-size:16px;" class="pull-right hidden-xs showopacity"></span></a></li>
				<li class="active"><a href="#">PHP - DECISION MAKING<span style="font-size:16px;" class="pull-right hidden-xs showopacity"></span></a></li>
				<li class="active"><a href="#">PHP - LOOP TYPES<span style="font-size:16px;" class="pull-right hidden-xs showopacity"></span></a></li>
				
			</ul>
		</div>
	
</nav>
			
		</div>
		<div class="col-sm-9">
			<div>
				<h1 style=" border-bottom: 1px solid; text-shadow: 2px 2px 3px #ccc;
    text-align: center;">PHP TUTORIAL</h1>
    <div>
    <h3 style="border-bottom: 1PX solid; margin-top: 20px;">PHP INTRODUCTION:-</h3>
				<p style="font-size: 18px!important; text-align: justify;">The PHP Hypertext Preprocessor (PHP) is a programming language that allows web developers to create dynamic content that interacts with databases. PHP is basically used for developing web based software applications. This tutorial helps you to build your base with PHP.</p>	
			</div>
			<div>
					    <h3 style="border-bottom: 1PX solid; margin-top: 20px;">PHP - SYNTAX:-</h3>
									<p style="font-size: 18px!important; text-align: justify;">
										<h3 style="text-shadow: 2px 2px 3px #ccc; margin-top: 20px;">Escaping to PHP:-</h3>
					The PHP parsing engine needs a way to differentiate PHP code from other elements in the page. The mechanism for doing so is known as 'escaping to PHP'. There are four ways to do this −

					<h3 style="text-shadow: 2px 2px 3px #ccc; margin-top: 20px;">Canonical PHP tags:-</h3>
					The most universally effective PHP tag style is −

					<pre><h5 style="color: black;"><?php ?></h5></pre>
					If you use this style, you can be positive that your tags will always be correctly interpreted.

					<h3 style="text-shadow: 2px 2px 3px #ccc; margin-top: 20px;">Short-open (SGML-style) tags</h3>
					Short or short-open tags look like this −

					<pre><h5 style="color: black;"><?  ?></h5></pre>
					Short tags are, as one might expect, the shortest option You must do one of two things to enable PHP to recognize the tags −<br><br>

					Choose the --enable-short-tags configuration option when you're building PHP.<br><br>

					Set the short_open_tag setting in your php.ini file to on. This option must be disabled to parse XML with PHP because the same syntax is used for XML tags.</p>	
								</div>
			<div>
    <h3 style="border-bottom: 1PX solid; margin-top: 20px;">PHP - VARIABLE:-</h3>
				<p style="font-size: 18px!important; text-align: justify;">The PHP Hypertext Preprocessor (PHP) is a programming language that allows web developers to create dynamic content that interacts with databases. PHP is basically used for developing web based software applications. This tutorial helps you to build your base with PHP.</p>	
			</div>
			<div>
    <h3 style="border-bottom: 1PX solid; margin-top: 20px;">PHP - CONSTANT:-</h3>
				<p style="font-size: 18px!important; text-align: justify;">The PHP Hypertext Preprocessor (PHP) is a programming language that allows web developers to create dynamic content that interacts with databases. PHP is basically used for developing web based software applications. This tutorial helps you to build your base with PHP.</p>	
			</div>
			<div>
    <h3 style="border-bottom: 1PX solid; margin-top: 20px;">PHP - OPERATOR TYPES:-</h3>
				<p style="font-size: 18px!important; text-align: justify;">The PHP Hypertext Preprocessor (PHP) is a programming language that allows web developers to create dynamic content that interacts with databases. PHP is basically used for developing web based software applications. This tutorial helps you to build your base with PHP.</p>	
			</div>
			<div>
    <h3 style="border-bottom: 1PX solid; margin-top: 20px;">PHP - DECISION MAKING:-</h3>
				<p style="font-size: 18px!important; text-align: justify;">The PHP Hypertext Preprocessor (PHP) is a programming language that allows web developers to create dynamic content that interacts with databases. PHP is basically used for developing web based software applications. This tutorial helps you to build your base with PHP.</p>	
			</div>
			<div>
    <h3 style="border-bottom: 1PX solid; margin-top: 20px;">PHP - LOOP TYPES:-</h3>
				<p style="font-size: 18px!important; text-align: justify;">The PHP Hypertext Preprocessor (PHP) is a programming language that allows web developers to create dynamic content that interacts with databases. PHP is basically used for developing web based software applications. This tutorial helps you to build your base with PHP.</p>	
			</div>
			</div>
		</div>
	</div>
	</div>
	
</div>


<!-- footer -->

<div class="contact-w3ls ">
<div class="contact-top-w3-agile">
</div>
	<div class="container">
		<h2 class="heading-agileinfo white-w3ls">Contact Us<span class="black-w3ls">Welcome to our Emphasize. We are glad to have you around.</span></h2>
		<ul class="w3_address">
			<li><i class="fa fa-map-marker" aria-hidden="true"></i><span>Melbourne St, QLD 4101, Australia.</span></li>
			<li><i class="fa fa-volume-control-phone" aria-hidden="true"></i><span>+1234 567 567<br>+1567 567 234</span></li>
			<li><i class="fa fa-envelope-o" aria-hidden="true"></i><span><a href="mailto:info@example.com">info@example1.com</a><br><a href="mailto:info@example.com">info@example2.com</a></span></li>
			<li><i class="fa fa-comments-o" aria-hidden="true"></i><span><a href="contact.html">Contact >></a></span></li>
		</ul>
			<div class="clearfix"></div>
		<div class="copy">
				<ul class="banner-menu-w3layouts">
					<li><a href="index.html">Home</a></li>
					<li><a href="about.html">About</a></li>
					<li><a href="services.html">Services</a></li>
					<li><a href="gallery.html">Gallery</a></li>
					<li><a href="contact.html">Contact</a></li>
				</ul>
				<ul class="agileits_social_list">
					<li><a href="#" class="w3_agile_facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
					<li><a href="#" class="agile_twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
					<li><a href="#" class="w3_agile_dribble"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
					<li><a href="#" class="w3_agile_vimeo"><i class="fa fa-vimeo" aria-hidden="true"></i></a></li>
				</ul>
				<p>© 2017 Emphasize . All Rights Reserved | Design by <a href="http://w3layouts.com/">W3layouts</a> </p>
		</div>
	</div>
</div>
<!--//footer -->
<!-- js -->
<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
<!--//js -->
<script src="js/SmoothScroll.min.js"></script>
<!--Scrolling-top -->
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
</script>
<!--//Scrolling-top -->
<!-- smooth scrolling -->
	<script type="text/javascript">
		$(document).ready(function() {
		/*
			var defaults = {
			containerID: 'toTop', // fading element id
			containerHoverID: 'toTopHover', // fading element hover id
			scrollSpeed: 1200,
			easingType: 'linear' 
			};
		*/								
		$().UItoTop({ easingType: 'easeOutQuart' });
		});
	</script>
	<a href="#home" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
<!-- //smooth scrolling -->
<script type="text/javascript" src="js/bootstrap-3.1.1.min.js"></script>
</body>
</html>